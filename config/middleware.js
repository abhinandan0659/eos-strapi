module.exports = {
    settings: {
      cors: {
        origin: ['http://localhost:3000', 'http://localhost:1337', 
        'https://eos-strapi-dev.herokuapp.com', 'https://eos-strapi.herokuapp.com',
        'https://www.eosdesignsystem.com', 'https://suse.eosdesignsystem.com', 'https://userstory.eosdesignsystem.com',
        'http://eos-dev-room.herokuapp.com', 'http://eos-staging.herokuapp.com']
      }
    }
  };